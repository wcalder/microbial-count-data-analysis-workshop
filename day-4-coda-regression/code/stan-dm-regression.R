library(rstan)


#####################
## Data simulation ##
#####################

# simulate community data where the first
# five taxa respond to some covariate
source("./source-functions/sim-data-source.R")
set.seed(1234)
counts <- sim.dm(n_obs = 30,
                 n_vars = 2,
                 n_taxa = 10,
                 n_relevant_vars = 1,
                 n_relevant_taxa = 5,
                 beta_min = 0.5,
                 beta_max = 2,
                 signoise = 1.0,
                 n_reads_min = 5000,
                 n_reads_max = 10001
                 )

# take a look at what's in the object counts
# there is a list with a number of different 
# arrays

# first we have the simuated covariates in 
head(counts$X)

# next are the counts in 
head(counts$Y)

# the true beta coefficients are here
# where only the first 5 taxa respond to 
# first covariate
head(counts$betas)



#########################################################
## Dirichlet multinomial regression analysis with Stan ##
#########################################################

# normally you would standardize covariate with mean of zero 
# to improve mcmc sampling, but our simulated data function
# already standardized the covariates

# stan requires the data in a list
# see the code for dm-theta-regression.stan
# under the "data" section to see how these variables 
# are used

####################################################
# In the Stan code for data, we have the following #
####################################################
# data {
#   int<lower=1> N;  // total number of observations
#   int<lower=2> ncolY; // number of categories
#   int<lower=2> ncolX; // number of predictor levels
#   matrix[N,ncolX] X; // predictor design matrix
#   int Y[N,ncolY]; // response variable
#   real sd_prior; // Prior standard deviation
# }

stan_data <- list(N = nrow(counts$X), 
                  ncolY = ncol(counts$Y), 
                  ncolX = ncol(counts$X),
                  X = counts$X, 
                  Y = counts$Y, 
                  sd_prior = 2
                  )

# With stan_model(), use auto_write = T and it will save the compiled 
# model so you won't have to recompile it every time

#################################################
### First time through, run stan_model() below ### 
#################################################

# stan_dm <- stan_model(file = "day-4-coda-regression/code/dm-theta-regression.stan",
#                      auto_write = T)


# once you have compiled the file above you can comment out
# the above function and just run this readRDS instead

# stan_dm <- readRDS("day-4-coda-regression/code/dm-theta-regression.rds")


# generate posterior samples with a short run
fit <- sampling(stan_dm, data = stan_data, chains = 2, iter = 2000, cores = 2,
                 control = list(adapt_delta = 0.95, max_treedepth = 20),
                 refresh = 100, pars = c("beta", "theta","p"))

# I ran this previously and saved it as an R object
# save(fit, file = "day-4-coda-regression/code/fit.Rdata")
load(file = "day-4-coda-regression/code/fit.Rdata")

####################################
## Examining model fit and output ##
####################################


# the output from Stan is found in the object "fit"
# this can be an intimidating object to work with initially
# see the rstan documentation for functions that manipulate
# the model output, e.g. extact(fit)
# https://mc-stan.org/rstan/articles/stanfit_objects.html


# Extract the samples for the regression coefficients 
# from fit and find the point estimates

# what's in the summary output
# hint: what are the dimensions referring to?
fit.summary <- summary(fit)$summary



# What's the estimate for the regression coefficient
# for the 7th taxa and second beta (i.e. first covariate)
# and does the estimate include zero? 




# Can you say that a covariate isn't important if the 
# coefficient estimate includes zero?




## How do you interpret the regression coefficients?




########################################
## Is this a good model for the data? ##
########################################

# You already know things like R2 from the 
# point estimates of the regression coefficients
# but we have more than just the regression 
# model - we have a generative model for the 
# variation in counts via the Dirichlet and 
# multinomial. The regression model is just 
# one part of that whole model.

# How can we evaluate how well the entire model 
# fits our data?

# One way is called posterior predictive
# checks where you make predictions
# based on all of your model uncertainty.
# In Stan, you can generate predicted values
# and compare your data to the predictions
# using their block called generated quantities.

# For an exercise, let's do that using rdirichlet()
# and the regression model estimates

# we have 2000 samples from the posterior, so 
# you should en up with 2000 predictions

params <- extract(fit)

# what's in the object params? What do all these mean?
# Hint: look out our model


n.obs <- 30
n.samples <- 2000
n.otus <- 10
counts$X

# array to fill in our posterior predictions
post.pred <- array(data = NA, dim = c(2000, 30, 10))
for(i in 1:n.samples){
  for(j in 1:n.obs){
  # regression model
  # normalized with the softmax
  # then use rdirichlet to get a predictive estimate for p for that location
    
    }
}

true.prop <- counts$Y/rowSums(counts$Y)

hist(post.pred[,18,10])
mean(post.pred[,18,10])
true.prop[18,10]
# how can we compare our data to those predictions?

# what about with the quantile function?

