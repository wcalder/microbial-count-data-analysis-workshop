data {
  int<lower=1> N;  // total number of observations
  int<lower=2> ncolY; // number of categories
  int<lower=2> ncolX; // number of predictor levels
  matrix[N,ncolX] X; // predictor design matrix
  int Y[N,ncolY]; // response variable
  real sd_prior; // Prior standard deviation
}

parameters {
  matrix[ncolY-1,ncolX] beta_raw; // coefficients (raw)
  real<lower=0> theta;
  simplex[ncolY] p[N];
}

// We have to transform the regression coefficients so
// that one of the regression equations is set to zero:
transformed parameters{
  matrix[ncolY,ncolX] beta; // coefficients
  for (l in 1:ncolX) {
    beta[ncolY,l] = 0.0; // sets the last category as reference
    }
  for (k in 1:(ncolY-1)) {
    for (l in 1:ncolX) {
      beta[k,l] = beta_raw[k,l];
      }
} }

model {
  for (k in 1:(ncolY-1)) {
    for (l in 1:ncolX) {
      beta_raw[k,l] ~ normal(0,sd_prior);
      }
    }
  // likelihood
  for (n in 1:N) {
    vector[ncolY] logits;
    for (m in 1:ncolY){
            logits[m] = X[n,] * transpose(beta[m,]);
      }
    p[n,] ~ dirichlet(softmax(logits) * theta);
    Y[n,] ~ multinomial(p[n,]);
    }
}
